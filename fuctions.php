<?php
	function db(){
		return new PDO("mysql:host=localhost;dbname=upload","root","");
	}

	function insertPic($dir)
	{
		$db = db();
		$sql = "INSERT INTO pictures(picDir) VALUES(?)";
		$s = $db->prepare($sql);
		$s->execute(array($dir));
		$db = null;
	}

	function retrievePic()
	{
		$db = db();
		$sql = "SELECT * FROM pictures";
		$s = $db->query($sql)->fetchAll();
		$db = null;

		return $s;
	}

?>